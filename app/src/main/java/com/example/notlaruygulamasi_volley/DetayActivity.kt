package com.example.notlaruygulamasi_volley

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.notlaruygulamasi_volley.databinding.ActivityDetayBinding
import com.google.android.material.snackbar.Snackbar

class DetayActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetayBinding
    private lateinit var not:Notlar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityDetayBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding.toolbarNotDetay.title = "Not Detay"
        setSupportActionBar(binding.toolbarNotDetay)

        not = intent.getSerializableExtra("nesne") as Notlar
        binding.editTextDersAdi.setText(not.ders_adi)
        binding.editTextTextNot1.setText(not.not1.toString())
        binding.editTextTextNot2.setText(not.not2.toString())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_sil ->{
                Snackbar.make(binding.root,"Silinsin mi?",Snackbar.LENGTH_LONG).setAction("evet"){
                    notSil(not.not_id)
                    startActivity(Intent(this@DetayActivity,MainActivity::class.java))
                    finish()
                }.show()
                return true
            }
            R.id.action_duzenle->{

                val ders_adi = binding.editTextDersAdi.text.toString().trim()
                val not1 = binding.editTextTextNot1.text.toString().trim()
                val not2 = binding.editTextTextNot2.text.toString().trim()
                if (TextUtils.isEmpty(ders_adi)){
                    Snackbar.make(binding.toolbarNotDetay,"Ders adını giriniz",Snackbar.LENGTH_SHORT).show()
                    return false
                }
                if (TextUtils.isEmpty(not1)){
                    Snackbar.make(binding.toolbarNotDetay,"1. notu giriniz",Snackbar.LENGTH_SHORT).show()
                    return false
                }
                if (TextUtils.isEmpty(not2)){
                    Snackbar.make(binding.toolbarNotDetay,"2. notu giriniz",Snackbar.LENGTH_SHORT).show()
                    return false
                }
                notGuncelle(not.not_id,ders_adi,not1.toInt(),not2.toInt())
                startActivity(Intent(this@DetayActivity,MainActivity::class.java))
                finish()
                return true
            }
            else-> return false

        }
        return super.onOptionsItemSelected(item)
    }
    fun notGuncelle(not_id:Int,ders_adi:String,not1:Int,not2:Int){
        val url = "http://kasimadalan.pe.hu/notlar/update_not.php"
        val istek = object : StringRequest(Request.Method.POST,url,{ cevap->

        },{}){
            override fun getParams(): MutableMap<String, String>? {
                val params = HashMap<String,String>()
                params["not_id"] = not_id.toString()
                params["ders_adi"] = ders_adi
                params["not1"] = not1.toString()
                params["not2"] = not2.toString()
                return params

            }
        }
        Volley.newRequestQueue(this@DetayActivity).add(istek)
    }
    fun notSil(not_id:Int){
        val url = "http://kasimadalan.pe.hu/notlar/delete_not.php"
        val istek = object : StringRequest(Request.Method.POST,url,{ cevap->

        },{}){
            override fun getParams(): MutableMap<String, String>? {
                val params = HashMap<String,String>()
                params["not_id"] = not_id.toString()

                return params

            }
        }
        Volley.newRequestQueue(this@DetayActivity).add(istek)
    }
}