package com.example.notlaruygulamasi_volley

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.notlaruygulamasi_volley.databinding.ActivityNotKayitBinding
import com.google.android.material.snackbar.Snackbar

class NotKayit : AppCompatActivity() {
    private lateinit var binding: ActivityNotKayitBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityNotKayitBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding.toolbarNotKayit.title = "Not Kayıt"
        setSupportActionBar(binding.toolbarNotKayit)

        binding.buttonNotKayit.setOnClickListener{

            val ders_adi = binding.editTextDersAdi.text.toString().trim()
            val not1 = binding.editTextTextNot1.text.toString().trim()
            val not2 = binding.editTextTextNot2.text.toString().trim()

            if (TextUtils.isEmpty(ders_adi)){
                Snackbar.make(binding.toolbarNotKayit,"Ders adını giriniz",Snackbar.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(not1)){
                Snackbar.make(binding.toolbarNotKayit,"1. notu giriniz",Snackbar.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(not2)){
                Snackbar.make(binding.toolbarNotKayit,"2. notu giriniz",Snackbar.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            notKayit(ders_adi,not1.toInt(),not2.toInt())
            startActivity(Intent(this@NotKayit,MainActivity::class.java))
            finish()
        }
    }
    fun notKayit(ders_adi:String,not1:Int,not2:Int){
        val url = "http://kasimadalan.pe.hu/notlar/insert_not.php"
        val istek = object : StringRequest(Request.Method.POST,url,{cevap->

        },{}){
            override fun getParams(): MutableMap<String, String>? {
                val params = HashMap<String,String>()
                params["ders_adi"] = ders_adi
                params["not1"] = not1.toString()
                params["not2"] = not2.toString()
                return params

            }
        }
        Volley.newRequestQueue(this@NotKayit).add(istek)
    }
}